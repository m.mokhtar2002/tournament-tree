#pragma once
#include <stdio.h>
#include <iostream>
#include <iomanip>
#include <string>
#include "Queue.cpp"
using namespace std;

template <typename ElementType>
class TournamentTree {

private:
    /************PRIVATE MEMBERS*************/

    /********Node class, the key for creating the tree*********/
    class Node
    {
    public:

        /**********Node DATA MEMBERS*********/
        //-- each node carries an index.
        int index;
        //-- each node has a pointer to its left or/and right nodes.
        Node* left, * right;

        /******Node OPERATIONS******/
        //-- Default constrctor: initializes next member to Node()
        Node() : right(0), left(0)
        {}

        //-- Explicit-value constructor:  initializes the given index to index.
        Node(int index) : index(index), left(0), right(0)
        {}
    }; //--- end of Node class

    /***** Data Members *****/
    
    //-- myRoot is the pointer to the root of the tree (which represents the best node).
    Node* myRoot = NULL;

    //-- The storage of the tree , where each index has its own datavalue.
    ElementType* myArray;

    //-- This variable indicates the Size of the tree.
    int mySize;
    //-- This variable indicates the Capacity of the tree.
    int myCapacity;

    /***** Function Members *****/

    void constructTournamentTree();
    /*------------------------------------------------------------------------
     Constructs The TournamentTree nodes and allocates them according 
     the TournamentTree design.

     Precondition:  myArray is allocated containing values.
     Postcondition: All of the nodes are allocated and placed according to
     their values , where the maxima will be stored in myRoot.
     -----------------------------------------------------------------------*/

    void destructTournamentTree(Node*& root);
    /*------------------------------------------------------------------------
     Deallocates The TournamentTree nodes and points them to NULL

     Precondition:  None.
     Postcondition: TournamentTree nodes are deallocated and myRoot is set 
     to NULL.
     -----------------------------------------------------------------------*/

    void traverseHeight(Node* root, ElementType arr[], ElementType& res, int& minimumgames);
    /*------------------------------------------------------------------------
     Traverses the TournamentTree to find the second best.

     Precondition:  TournamentTree has at least 2 indicies.
     Postcondition: Sets res to the value of second best , and minimimumgames
     to games required to find the second best.
     -----------------------------------------------------------------------*/

    void displayTreeHorizontallyAux(ostream& out, Node* root, int spaces);
    /*------------------------------------------------------------------------
     Display values stored in the TournamentTree in horizontal manner.

     Precondition:  ostream out is open.
     Postcondition: TournamentTree's contents, from Root node to leaf nodes
     have been output to out in left to right display manner.
     -----------------------------------------------------------------------*/

    void RedrawTree();
    /*------------------------------------------------------------------------
     Reconstructs the tree according to their values 
     to meet the TournamentTree design.

     Precondition:  None.
     Postcondition: TournamentTree is reconstructed , where each node
     is allocated according to its value.
     -----------------------------------------------------------------------*/

    string centered(string const& str, int width);
    /*------------------------------------------------------------------------
     Adds spaces at front and back of a string, to have a center margin.

     Precondition:  None.
     Postcondition: String is intialized in center margin.
     -----------------------------------------------------------------------*/
   

public:
    /******PUBLIC MEMBERS ***********/

    /***** Function Members *****/

    //-- Tree constructors.

    TournamentTree();
    /*------------------------------------------------------------------------
     Construct a TournamentTree object.

     Precondition:  None.
     Postcondition: An empty TournamentTree object has been constructed (myRoot is
     initialized to NULL and myArray is an array with capacity
     of 1000) elements of type ElementType).
     -----------------------------------------------------------------------*/

    TournamentTree(int capacity);
    /*------------------------------------------------------------------------
     Construct a TournamentTree object.

     Precondition:  None.
     Postcondition: An empty TournamentTree object has been constructed (myRoot is
     initialized to NULL and myArray is an array with myCapacity of capacity
     elements of type ElementType).
     -----------------------------------------------------------------------*/

    TournamentTree(ElementType arr[], int size, int capacity);
    /*------------------------------------------------------------------------
     Construct a TournamentTree object.

     Precondition:  None.
     Postcondition: A TournamentTree object has been constructed and drawn
     (according to the data given by arr and myArray is intialized to arr).
     -----------------------------------------------------------------------*/

    TournamentTree(TournamentTree& origTree);
    /*------------------------------------------------------------------------
     Copy Constructor

     Precondition:  origTree is the TournamentTree to be copied and is received as
     a const reference parameter.
     Postcondition: A copy of origTree has been constructed.
     ------------------------------------------------------------------------*/

    TournamentTree(TournamentTree& origTree, int capacity);  
    /*------------------------------------------------------------------------
     Copy Constructor

     Precondition:  origTree is the TournamentTree to be copied and is received as
     a const reference parameter.
     Postcondition: A copy of origTree has been constructed with given capacity
     (myCapacity is intializied to capacity as long it's bigger than origTree).
     ------------------------------------------------------------------------*/

     /***** Destructor *****/

    ~TournamentTree();
    /*-----------------------------------------------------------------------
     Class destructor

     Precondition:  None
     Postcondition: The dynamic array and the nodes in the TournamentTree
     has been deallocated.
     ------------------------------------------------------------------------*/

    void insertElement(ElementType Element);
    /*-----------------------------------------------------------------------
     Insert an element in the tree

     Precondition:  value is to be added to this TournamentTree
     Postcondition: value is added to TournamenTree and the tree is redrawn
     provided there is space ; otherwise, the array's capacity is doubled
     ------------------------------------------------------------------------*/

    void deleteElement(int index);
    /*------------------------------------------------------------------------
     Remove index from TournamentTree (if any).

     Precondition:  TournamentTree is nonempty and index is valid.
     Postcondition: Index from TournamentTree has been removed, unless the
     TournamentTree is empty or index is invalid ; in that case, 
     an error message is displayed.
     -----------------------------------------------------------------------*/

    ElementType getBest();
    /*------------------------------------------------------------------------
     Retrieve value at myRoot node (the winner of the TournamentTree).

     Precondition:  TournamentTree is nonempty
     Postcondition: Value of myRoot of TournamentTree is returned,
     unless the TournamentTree is empty; in that case,
     an error message is displayed and a "garbage value" is returned.
     -----------------------------------------------------------------------*/

    ElementType getSecondBest();
    /*------------------------------------------------------------------------
     Retrieve the second maximum value in the TournamentTree.

     Precondition:  TournamentTree contains more than one index.
     Postcondition: Value of second maximum of TournamentTree is returned,
     unless the TournamentTree contains less than two index ; in that case,
     an error message is displayed and a "garbage value" is returned.
     -----------------------------------------------------------------------*/

    void displayTreeHorizontally(ostream& out);
    /*------------------------------------------------------------------------
     Display values stored in the TournamentTree in horizontal manner.

     Precondition:  ostream out is open.
     Postcondition: TournamentTree's contents, from Root node to leaf nodes 
     have been output to out in left to right display manner.
     -----------------------------------------------------------------------*/

    void displayTreeVertically(ostream& out);
    /*------------------------------------------------------------------------
     Display values stored in the TournamentTree in vertical manner.

     Precondition:  ostream out is open.
     Postcondition: TournamentTree's contents, from Root node to leaf nodes 
     have been output to out in from top to bottom manner.
     -----------------------------------------------------------------------*/

    void displayBest(ostream& out) const;
    /*------------------------------------------------------------------------
     Display value of myRoot (the winner of the TournamentTree).

     Precondition:  TournamentTree has at least 1 index.
     Postcondition: myRoot content has been output to out.
     -----------------------------------------------------------------------*/

    void displaySecondBest(ostream& out);
    /*------------------------------------------------------------------------
     Displays second maximum value in the TournamentTree.

     Precondition:  ostream out is open.
     Postcondition: Second maximum value has been output to out.
     -----------------------------------------------------------------------*/

    void displayFirst_SecondBest(ostream& out);
    /*------------------------------------------------------------------------
     Displays myRoot value (the winner of the tree) and
     second maximum value in the TournamentTree.

     Precondition:  ostream out is open.
     Postcondition: myRoot value and Second maximum value
     has been output to out.
     -----------------------------------------------------------------------*/

};  //--- end of Tree

//====== FUNCTION DEFINITIONS ======

/***************Private Function Members *****************/

//--- Definition of constructTournamentTree()
template<typename ElementType>
void TournamentTree<ElementType>::constructTournamentTree() {
    // Create a list to store nodes of current level 
    Queue<Node*> q;
    Node* root = NULL;
    for (int i = 0; i < mySize; i += 2)
    {
        Node* t1 = new Node(i);
        Node* t2 = NULL;
        if (i + 1 < mySize)
        {
            // Make a node for next element 
            t2 = new Node(i + 1);

            // Make bigger of two as root 
            root = (myArray[i] > myArray[i + 1]) ? new Node(i) : new Node(i + 1);

            // Make two nodes as children of bigger 
            root->left = t1;
            root->right = t2;

            // Add root
            q.enqueue(root);
        }
        else {
            if (root == NULL)
                root = t1;
            q.enqueue(t1);
        }
    }
    // Construct the complete tournament tree from above 
    // prepared list of winners in first round. 
    while (q.size() > 1)
    {
        // Find index of last pair
        int last = (q.size() % 2 == 1) ? (q.size() - 2) : (q.size() - 1);

        // Process current list items in pair 
        for (int i = 0; i < last; i += 2)
        {
            // Extract two nodes from list, make a new 
            // node for winner of two 
            Node* f1 = q.front();
            q.dequeue();

            Node* f2 = q.front();
            q.dequeue();
            root = (myArray[f1->index] > myArray[f2->index]) ?
                new Node(f1->index) : new Node(f2->index);

            // Make winner as parent of two 
            root->left = f1;
            root->right = f2;

            // Add winner to list of next level 
            q.enqueue(root);
        }
        //..
        if (q.size() % 2 == 1)
        {
            q.enqueue(q.front());
            q.dequeue();
        }
    }
    //Update the tree root.
    myRoot = root;
}

//--- Definition of destructTournamentTree()
template<typename ElementType>
void TournamentTree<ElementType>::destructTournamentTree(Node*& root) {
    if (root == NULL) {
        return;
    }
    if (root->left != NULL)
        destructTournamentTree(root->left);
    if (root->right != NULL)
        destructTournamentTree(root->right);
    delete (root);
    root = NULL;
}

//--- Definition of traverseHeight()
template <typename ElementType>
void  TournamentTree<ElementType>::traverseHeight(Node* root, ElementType arr[], ElementType& res, int& minimumgames) {
    // Base case
    if (root == NULL || (root->left == NULL && root->right == NULL))
        return;

    minimumgames++;
    // If left child is greater than current result, 
    // update result and recur for left subarray. 
    if (res < arr[root->left->index] && root->left->index != root->index)
    {
        res = arr[root->left->index];
        traverseHeight(root->right, arr, res, minimumgames);
    }
    // If right child is greater than current result, 
    // update result and recur for left subarray. 
    else if (res < arr[root->right->index] && root->right->index != root->index)
    {
        res = arr[root->right->index];
        traverseHeight(root->left, arr, res, minimumgames);
    }
    else if (root->left->index == root->index)
        traverseHeight(root->left, arr, res, minimumgames);
    else if (root->right->index == root->index)
        traverseHeight(root->right, arr, res, minimumgames);
}

//--- Definition of RedrawTree()
template <typename ElementType>
void TournamentTree<ElementType>::RedrawTree() {
    destructTournamentTree(myRoot);
    constructTournamentTree();
}

//--- Definition of centered()
template <typename ElementType>
string TournamentTree<ElementType>::centered(string const& str, int width) // used for centering display
{
    int len = str.length();
    if (width < len) { return str; }

    int diff = width - len;
    int pad1 = diff / 2;
    int pad2 = diff - pad1;
    return string(pad1, ' ') + str + string(pad2, ' ');
}

/******Public Function Members ***********/

//--- Definition of TournamentTree constructor
template <typename ElementType>
TournamentTree<ElementType>::TournamentTree()
    : mySize(0), myCapacity(1000)
{ 
    myArray = new ElementType[myCapacity];
}

//--- Definition of TournamentTree constructor with capacity
template <typename ElementType>
TournamentTree<ElementType>::TournamentTree(int capacity)
    : mySize(0), myCapacity(capacity)
{ 
    myArray = new ElementType[myCapacity];
}

//--- Definition of TournamentTree array constructor
template <typename ElementType>
TournamentTree<ElementType>::TournamentTree(ElementType arr[], int size, int capacity)
    : mySize(size), myCapacity(capacity)
{
    myArray = new ElementType[myCapacity];
    for (int i = 0; i < mySize; i++)
        myArray[i] = arr[i];

    constructTournamentTree();
}

//--- Definition of TournamentTree copy constructor
template <typename ElementType>
TournamentTree<ElementType>::TournamentTree(TournamentTree& origTree)
    : mySize(origTree.mySize), myCapacity(origTree.myCapacity)
{
    myArray = new ElementType[myCapacity];
    for (int i = 0; i < mySize; i++) {
        myArray[i] = origTree.myArray[i];
    }
    constructTournamentTree();
}

//--- Definition of TournamentTree copy constructor with capacity
template <typename ElementType>
TournamentTree<ElementType>::TournamentTree(TournamentTree& origTree, int capacity)
{
    if (capacity < origTree.mySize)
    {
        cerr << "Capacity less than needed size";
        return;
    }
    mySize = origTree.mySize;
    myCapacity = capacity;

    myArray = new ElementType[myCapacity];
    for (int i = 0; i < mySize; i++) {
        myArray[i] = origTree.myArray[i];
    }
    constructTournamentTree();
}

//--- Definition of TournamentTree destructor
template <typename ElementType>
TournamentTree<ElementType>::~TournamentTree() {
    delete myArray;
    myArray = NULL;
    destructTournamentTree(myRoot);
}

//--- Definition of insertElement()
template <typename ElementType>
void TournamentTree<ElementType>::insertElement(ElementType Element) {
    if (mySize == myCapacity)
    {
        ElementType* newArray = new ElementType[myCapacity * 2];
        for (int i = 0; i < mySize; i++) {
            newArray[i] = myArray[i];
        }

        delete myArray;
        myArray = newArray;
        myCapacity *= 2;
    }

    myArray[mySize] = Element;
    mySize++;
    RedrawTree();
}

//--- Definition of deleteElement()
template <typename ElementType>
void TournamentTree<ElementType>::deleteElement(int index) {
    if (myRoot == NULL) {
        cerr << "The Tree is empty" << endl;
        return;
    }
    if (index >= mySize)
    {
        cerr << "Invalid index input" << endl;
        return;
    }
    if (index < 0) {
        cerr << "Index can't be less than zero " << endl;
        return;
    }

    for (index; index < mySize - 1; index++) {
        myArray[index] = myArray[index + 1];
    }
    mySize--;
    RedrawTree();
}

//--- Definition of getBest()
template <typename ElementType>
ElementType TournamentTree<ElementType>::getBest()
{
    //if tree is not empty, return the root value.
    if (myRoot != NULL)
    {
        return myArray[myRoot->index];
    }
    //if the tree is empty.
    else
    {
        cerr << "Error the tree is empty! , returning garbage" << endl;
        ElementType* garbage = new ElementType;
        return *garbage;
    }
}

//--- Definition of getSecondBest()
template <typename ElementType>
ElementType TournamentTree<ElementType>::getSecondBest() {
    // Traverse tree from root to find second best. 
    int minimumgames = 0;
    ElementType res = numeric_limits<ElementType>::min();
    traverseHeight(myRoot, myArray, res, minimumgames);
    minimumgames--;
    if (minimumgames > -1) {
        return res;
    }
    else if (minimumgames == -1) {
        cerr << "There is no second best , returning garbage " << endl;
        ElementType* garbage = new ElementType;
        return *garbage;
    }
}

/*******Display Functions***********/

//--- Definition of displayTreeHorizontally()
template <typename ElementType>
void TournamentTree<ElementType>::displayTreeHorizontally(ostream& out) {
    displayTreeHorizontallyAux(out, myRoot, mySize);
}

//--- Definition of displayTreeHorizontallyAux()
template <typename ElementType>
void TournamentTree<ElementType>::displayTreeHorizontallyAux(ostream& out, Node* root, int spaces) {
    if (root != NULL) {
        displayTreeHorizontallyAux(out, root->right, spaces + 5);
        out << string(spaces, ' ');
        out << "   " << myArray[root->index] << endl;
        displayTreeHorizontallyAux(out, root->left, spaces + 5);
    }
}

//--- Definition of displayTreeVertically()
template <typename ElementType>
void TournamentTree<ElementType>::displayTreeVertically(ostream& out){
    if (myRoot == NULL) {
        cerr << "The tree is empty" << endl;
        return;
    }
    string s;
    s.clear();
    Queue<Node*> q;
    q.enqueue(myRoot);

    for (int i = 0; q.size() != 0; i++)
    {
        int size = q.size();
        for (int j = 0; j < size; j++)
        {
            s += to_string(myArray[q.front()->index]) + ' ';
            if (q.front()->left != NULL) q.enqueue(q.front()->left);
            if (q.front()->right != NULL) q.enqueue(q.front()->right);
            q.dequeue();
        }
        out << centered(s, 600) << endl;
        s.clear();
    }
}

//--- Definition of displayBest()
template <typename ElementType>
void TournamentTree<ElementType>::displayBest(ostream& out) const {
    //if tree is not empty, return the root value.
    if (myRoot != NULL)
    {
        out << "Best (Maximum): " << myArray[myRoot->index] << endl;
    }
    //if the tree is empty.
    else
    {
        cerr << "Error the tree is empty!";
        ElementType* garbage = new ElementType;
        out << "Printing garbage " << garbage << endl;
    }
}

//--- Definition of displaySecondBest()
template <typename ElementType>
void TournamentTree<ElementType>::displaySecondBest(ostream& out) {
    // Traverse tree from root to find second best.
    int minimumgames = 0;
    ElementType res = numeric_limits<ElementType>::min();
    traverseHeight(myRoot, myArray, res, minimumgames);
    minimumgames--;
    if (minimumgames > -1) {
        out << ", Second best: " << res << endl;
        out << "Minimum games to find second best is  " << minimumgames << endl;
    }
    else if (minimumgames == -1)
        out << endl << "Can't display Second Best because there is less than two players in tournament" << endl;
}

//--- Definition of displayFirst_SecondBest()
template <typename ElementType>
void TournamentTree<ElementType>::displayFirst_SecondBest(ostream& out) {
    // Traverse tree from root to find second best 
    int minimumgames = 0;
    ElementType res = numeric_limits<ElementType>::min();
    traverseHeight(myRoot, myArray, res, minimumgames);
    minimumgames-- ;
    out << "Best (Maximum): " << myArray[myRoot->index];
        if (minimumgames > -1) {
            out << ", Second best: " << res << endl;
            out << "Minimum games to find second best is  " << minimumgames << endl;
        }
        else if (minimumgames == -1)
            out <<endl<< "Can't display Second Best because there is less than two players in tournament" << endl;
}