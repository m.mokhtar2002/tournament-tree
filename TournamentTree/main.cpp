#include <iostream>
#include "TournamentTree.cpp"
using namespace std;

// Function used to reduce line of codes for deleting.
template <typename ElementType>
void delete_elm(TournamentTree<ElementType> &tree,int index, ElementType Element) {
    cout << "Deleting index (" << Element <<") from tree 5" <<endl;
    tree.deleteElement(index);
    cout << "Printing tree Vertically \n" << endl;
    tree.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree.displayTreeHorizontally(cout);
    cout << endl;
}

int main()
{
    
    /********TEST ONE**********/
    cout << "\nFIRST TEST \n\n\n";
    int arr1[] = { 4,5,1,3,0,1,6,7 };
    TournamentTree<int>* tree1 = new TournamentTree<int>(arr1, sizeof(arr1) / sizeof(int), 50);

    tree1->displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    tree1->displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree1->displayTreeHorizontally(cout);
    cout << endl;

    cout << "COPY CONSTRUCTOR same as tree1\n";
    TournamentTree<int> copied(*tree1);
    delete tree1;

    copied.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    copied.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    copied.displayTreeHorizontally(cout);
    cout << endl;


    /********TEST TWO**********/
    cout << "\nSECOND TEST \n\n\n";
    int arr2[] = {50,49,30,20,10};
    TournamentTree<int> tree2(arr2, sizeof(arr2) / sizeof(int), 50);


    tree2.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    tree2.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree2.displayTreeHorizontally(cout);
    cout << endl;


    /*********TEST THREE************/
    cout << "\nTHIRD TEST \n\n\n";
    //-- sorted array.
    int arr3[] = { 1,2,3,4,5,6,7,8,9,10 };
    TournamentTree<int> tree3(arr3, sizeof(arr3) / sizeof(int), 50);
    tree3.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    tree3.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree3.displayTreeHorizontally(cout);
    cout << endl;

    /*********TEST FOUR************/
    cout << "\nFOURTH TEST \n\n\n";
    int arr4[] = { -1,-20,-31,-410,-5,-6,-741,-820,-9,10 };
    TournamentTree<int> tree4(arr4, sizeof(arr4) / sizeof(int), 50);
    tree4.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    tree4.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree4.displayTreeHorizontally(cout);
    cout << endl;

    /*********TEST FIVE************/
    cout << "\nFIFTH TEST \n\n\n";
     TournamentTree<int> tree5;
     tree5.insertElement(-1);
     tree5.insertElement(-20);
     tree5.insertElement(-31);
     tree5.insertElement(-410);
     tree5.insertElement(-5);
     tree5.insertElement(-6);
     tree5.insertElement(-741);
     tree5.insertElement(-820);
     tree5.insertElement(-9);
     tree5.insertElement(10);

    tree5.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically\n" << endl;
    tree5.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree5.displayTreeHorizontally(cout);
    cout << endl;
    
    /*********TEST SIX************/
    cout << "\nSIXTH TEST \n\n\n";
    
    delete_elm<int>(tree5, 0, -1);
    delete_elm<int>(tree5, 0, -20);
    delete_elm<int>(tree5, 0, -31);
    delete_elm<int>(tree5, 0, -410);
    delete_elm<int>(tree5, 0, -5);
    delete_elm<int>(tree5, 0, -6);
    delete_elm<int>(tree5, 0, -741);
    delete_elm<int>(tree5, 0, -820);
    delete_elm<int>(tree5, 0, -9);
    delete_elm<int>(tree5, 0, 10);

    tree5.deleteElement(0);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree5.displayTreeHorizontally(cout);

    double best1 = tree5.getBest();
    cout << "Displaying returned best " << best1 << endl;
    
    cout << "Inserting Element 50" << endl;
    tree5.insertElement(50);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree5.displayTreeHorizontally(cout);

    tree5.insertElement(20);
    cout << "Inserting Element 20" << endl;
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree5.displayTreeHorizontally(cout);

    /*********TEST SEVEN************/
    cout << "\nSEVENTH TEST \n\n\n";
    double arr6[] = { 4.0,5.1,10.212};
    TournamentTree<double> tree6(arr6, sizeof(arr6) / sizeof(double), 50);

    tree6.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    tree6.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree6.displayTreeHorizontally(cout);
    cout << endl;

    double best = tree6.getBest();
    cout <<"Displaying returned best " << best << endl;

    /*********TEST EIGHT************/
    cout << "\nEIGHTH TEST \n\n\n";
    double arr7[] = {4.0};
    TournamentTree<double> tree7(arr7, sizeof(arr7) / sizeof(double), 100);

    tree7.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    tree7.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree7.displayTreeHorizontally(cout);
    cout << endl;
    
    double secbest = tree7.getSecondBest();
    cout << secbest << endl;

    /*********TEST NINE************/
    cout << "\nNINTH TEST \n\n\n";
    int arr8[] = { 8, 9 };
    TournamentTree<int> tree8(arr8, sizeof(arr8) / sizeof(int), 2);

    tree8.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    tree8.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree8.displayTreeHorizontally(cout);
    cout << endl;

    tree8.insertElement(10);
    tree8.displayFirst_SecondBest(cout);
    cout << "Printing tree Vertically \n" << endl;
    tree8.displayTreeVertically(cout);
    cout << "\nPrinting tree Horizontally \n" << endl;
    tree8.displayTreeHorizontally(cout);
    cout << endl;

    return 0;
}